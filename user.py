from tkinter import * 
from tkinter.filedialog import * 
 
class interface: 
    def __init__(self, root, menu, canvas): 
        self.root=root 
        self.menu=menu 
        self.canvas=canvas 
 
    def creer(self): 
        self.root.title("Interface TP") 
        self.root.geometry("600x600")
        self.root.config(background='#41B77F') 
 
    def espace(self): 
        self.canvas= Canvas(self.root, width=350, height=250, bg="blue") 
        self.canvas.grid(row=0, column=0) 
 
    def new(self): 
        fils = Toplevel(self.root) 
        fils.attributes("-topmost", 1) 
        self.canvas=Canvas(fils) 
        self.canvas.pack(expand=True, fill='both') 
        self.occ=[] 
 
    

    def sommet(self): 
        self.canvas.bind_all("<Button-1>", self.point) 
 
    def arrete(self): 
        self.canvas.bind_all('<Button-1>', self.segment) 

    
 
    def point(self, event): 
        i.set(i.get() + 1) 
        global x, y, circ, r 
        r = 15  # circle diameter 
        x, y = event.x, event.y  # clicked position 
        center_x, center_y = ((x - r) + (x + r)) / 2, ((y - r) + (y + r)) / 2 
        circ = self.canvas.create_oval(x - r, y - r, x + r, y + r, fill="blue", outline="blue")  # print circle 
        self.canvas.create_text(center_x, center_y, text=str(i.get())) 
 
    def segment(self, event): 
        global click_number 
        global x1, y1 
        if click_number == 0: 
            x1, y1 = event.x, event.y 
            click_number = 1 
        else: 
            x2, y2 = event.x, event.y 
            click_number = 0 
            self.canvas.create_line(x1, y1, x2, y2, fill="blue", width=1) 
 
    def open_file(self): 
        self.file = askopenfilename(title="Choisissez un ficher à ouvrir", 
                               filetypes=[("fichiers python", ".py"), ("tous les fichiers", ".*")]) 
 
    def enregistrer(self): 
        self.file = asksaveasfile(defaultextension=".py", 
                                                    filetypes=(('Python Files', '*.py'), ('ALL FILES', '*.*'))) 
 
    def saveas(self): 
        self.file = asksaveasfilename(title="Enregistrer sous ...", defaultextension=".py", 
                             filetypes=[("python file", ".py"), ("All files", ".*")]) 
 
    def menubar(self): 
        menub = Menu(self.root) 
 
        menu1 = Menu(menub, tearoff=0) 
        menu1.add_command(label="Nouveau",accelerator="Ctrl+N", command=self.new) 
        menu1.bind_all("<Control-N>", self.new) 
        menu1.add_command(label="Ouvrir....", accelerator="Ctrl+O", command=self.open_file) 
        menu1.bind_all("<Control-O>", self.open_file) 
        menu1.add_command(label="Enregistrer", accelerator="Ctrl+S", command=self.enregistrer) 
        menu1.bind_all("<Control-S>", self.enregistrer) 
        menu1.add_command(label="Enregistrer sous.....", accelerator="Ctrl+E",command=self.saveas) 
        menu1.bind_all("<Control-E>", self.saveas) 
        menu1.add_separator() 
        menu1.add_command(label="Fermer", command=self.root.quit) 
        menub.add_cascade(label="Fichier", menu=menu1) 
 
        menu2 = Menu(menub, tearoff=0) 
        menu2.add_command(label="Sommet", command=self.sommet) 
        menu2.add_command(label="Arete", command= self.arrete) 
 
        menub.add_cascade(label="Création", menu=menu2) 
 
        menu3 = Menu(menub, tearoff=0) 
        menu3.add_command(label="Graphe") 
        menu3.add_command(label="Chaines") 
        menu3.add_command(label="Matrices") 
 
        menub.add_cascade(label="Affichage", menu=menu3) 
 
        menu4 = Menu(menub, tearoff=0) 
        menu4.add_command(label="Parcours plus court chemein") 
        menu4.add_command(label="Coloration") 
 
        menub.add_cascade(label="Execution", menu=menu4) 
 
        menu5 = Menu(menub, tearoff=0) 
        menu5.add_command(label="Graphe") 
 
        menub.add_cascade(label="Edition", menu=menu5)
       
       
        
        
 
        self.root.config(menu=menub) 
 
    def affiche(self): 
        self.root.mainloop() 
 
inter = Tk() 
i=IntVar() 
menu = Menu() 
can= Canvas() 
 
app = interface(inter, menu, can) 
click_number=0 
app.creer() 
app.menubar() 
app.affiche()