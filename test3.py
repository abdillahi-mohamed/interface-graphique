# -*-coding: Utf-8-*-
from tkinter import *
from tkinter.filedialog import *
import os
from os.path import *  # pour récupèrer le nom des fichiers.


class Note():


        '''classe pour créer notre GUI'''
        def __init__(self):
         
                self.win = Tk()
                self.win.title("NoteBook")  # titre de l'application
                self.win.geometry("500x500")  # taille de l'application
                self.win.iconbitmap('icone.ico')  # logo de l'application
         
                # barre de menu
         
                self.menubar = Menu(self.win)
         
                # menu option
         
                self.option = Menu(self.menubar)
                self.option.add_command(label="nouveau", command=self.new_windows)
                self.option.add_command(label="ouvrir", command=self.open)
                self.option.add_command(label="Enregistrer sous", command=self.save_as)
                self.option.add_command(label="supprimer")
         
                # ajoute du menu à menubar.
         
                self.menubar.add_cascade(label="option", menu=self.option)
         
                # affichage du menu
         
                self.win.config(menu=self.menubar)
     
        def run(self):
                """boucle pour lancer la fenêtre."""
                self.win.mainloop()
     
        def new_windows(self):
                """commande nouvelle fenêtre"""
                self.windows = Toplevel(self.win)  # nouvelle fenêtre
                self.windows.title("nouveau")
                self.windows.geometry("500x500")  # taille interface
                self.windows.iconbitmap('icone.ico')
     
        def open(self):
                  # ouverture d'un fichier
                fichier = askopenfile(filetypes=[("Text files", "*.txt")])
                # on range le texte du fichier dans une variable.
                txt = fichier.read()
                # os.basename récupère le nom du fichier
                titre = basename(fichier.name)
                                                                              # grâce à son attribut name qui donne le chemin.
     
         
                # nouvelle fenêtre dans le widget parent.
                self.windows = Toplevel(self.win)
                self.windows.title(titre)  # on utilise la variable titre.
                self.windows.geometry("500x500")  # taille interface
                self.windows.iconbitmap('icone.ico')  # logo de l'interface
         
                # Création de la même barre de menu
                # que pour la fenêtre parent.
         
                self.menubar = Menu(self.windows)
         
                # menu option
         
                self.option = Menu(self.menubar)
                self.option.add_command(label="nouveau", command=self.new_windows)
                self.option.add_command(label="ouvrir", command=self.open)
                self.option.add_command(label="Enregistrer sous", command=self.save_as)
                self.option.add_command(label="supprimer")
         
                # ajoute du menu à menubar.
         
                self.menubar.add_cascade(label="option", menu=self.option)
         
                # affichage du menu
         
                self.windows.config(menu=self.menubar)
         
                # création d'un widgets pour éditer du texte.
                self.text = Text(self.windows, height=500, width=500)
                self.text.insert(1.0, txt)  # affichage du texte du fichier.
                self.text.bind('<KeyPress>', self.new_text)
                self.text.pack()  # affichage du widgets
         
        def new_text(self, event):
                
                self.text = self.text.get(1.0, END)
         
        def save_as(self):
                
                self.text.asksaveasfilename(filetypes=[("Text files", "*.txt")])
